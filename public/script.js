input = '0';

calculateAndPut = function (input) {
  days = moment().endOf('month').diff(moment(), 'days') + 1;
  $('#days').text(days)

  result = Math.round(parseFloat(input) * 100 / days) / 100;

  $('#resultnumber').text(result);
  console.log(days);
};

upgradeView = function () {
  if (input === '') {
    input = '0';
  }
  $('#inputnumber').text(input);

  calculateAndPut(input);
};


inputNumber = function (number) {
  return function () {

    if (parseFloat(input) === 0) {
      input = number
    } else {
      input = input + number;
    }

    upgradeView();
  }
};

inputComma = function () {
  if (!input.includes(',')) {
    input = input + ','
    upgradeView();
  }
};


inputBackspace = function () {
  input = input.substring(0, input.length - 1);
  upgradeView();
};

addKeyboardsFunctions = function () {
  numbers = _.map( _.range(0, 10), ((n) => String(n)))

  _.each(numbers, function (number) {
    $('#input' + number).click(inputNumber(number))
  })

  $('#inputcomma').click(inputComma)
  $('#inputbackspace').click(inputBackspace)

};

rescale = function () {
  var siteWidth = 184;
  var scale = 1;

  document
    .querySelector('meta[name="viewport"]')
    .setAttribute('content', 'width=' + siteWidth + ', initial-scale=' + scale + '');
};

main = function () {
  upgradeView();
  addKeyboardsFunctions();
  rescale();
};

$(document).ready(main)
